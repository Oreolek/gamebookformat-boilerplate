all:
	~/Programming/gamebookformat/formatgamebook.py game.gamebook .temp/game.tex
	xelatex -aux-directory=.temp -output-directory=build .temp/game.tex
	xelatex -aux-directory=.temp -output-directory=build .temp/game.tex
	rm build/game.out build/game.log build/game.aux
